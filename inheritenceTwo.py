#Inheritance by overriding
class Parent(object):
    def watchingTV(self):
        print "We only watch tv at night"

class Child(Parent):
    def watchingTV(self):
        print "We can watch TV anytime"    

dad = Parent()
son = Child()

dad.watchingTV()
son.watchingTV()