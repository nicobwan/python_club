#Classes
#years
#Vibrainium,Technology
#Warrior,BlackPanther,Throne,T.Chaka
#Wakanda,African Tribes,Regiment

#Functions
#Fighting
#Visiting
#UnderCover
#Stealing
#Confirmations
#Assigments

class Vibrainuim(object):
    def __init__(self):
        print "Vibrainuim is a powerful metal"
        self.color = "Black"
        self.price = "Vibranium is an Expensive metal"

class technology(Vibrainuim):
    def __init__(self):
        print "Our technology here in Wakanda has gotten advanced due to vibrainuims"

    def creationOfweapons():
        print "We can create weapons"

class warrior(object):
    def __init__(self,name):
        self.name = name
        self.skill = "Stabbing"

    def marriage(self,wife):
        self.wife = wife
        print "Am a warrior married to %s " % self.wife

    def fighting(self,weapon,energy):
        self.weapon = weapon
        self.energy = energy
        print "A warrior needs %s to fight and %s energy to fight" % (weapon,energy)  

class BlackPanther(warrior):
    def __init__(self,name,skincolor,strength):
        self.name = name
        self.skincolor = skincolor
        self.strength  = strength
        super(BlackPanther,self).__init__(self.name)
        print "Am called %s am %s and i have alot of %s " % (name,skincolor,strength)
#Making the instance
Melissa = BlackPanther("Melissa","Brown","BicepPower") # this runs fine
#I want to access the skill attribute in the warrior class because ,i have inherited 
#it in the BlankPanther Class but when i use the object Melissa to fetch it 
print Melissa.skill
# i get this error
#Traceback (most recent call last):
#  File "Wakanda.py", line 52, in <module>
#    print Melissa.skill
#AttributeError: 'BlackPanther' object has no attribute 'skill'
#I thought if i inherited a class all the attributes would be present in the 
#child class

