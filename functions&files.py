from sys import argv

script, input_file = argv

#definition of functions
def print_all(f):
        print f.read()
def print_a_line(line_count, f):
	print line_count, f.readline()

current_file = open(input_file)

print "First let's print the whole file:\n"

print_all(current_file)
print "Let's print three lines:"
current_line = 1
print_a_line(current_line,current_file)
current_line = current_line + 1
print_a_line(current_line,current_file)
current_line = current_line + 1
#Calls a function called print_a_line with arguments current_line and current_file
print_a_line(current_line, current_file)
