from sys import argv

from os.path import exists

script,from_file, to_file = argv

print"Copying from %s to %s" % (from_file,to_file)

#we could do these two on one line too,how?
in_file = open(from_file) ; indata = in_file.read()

print "The input file is %d bytes long" % len(indata)

print "Does the output file exist? %r" % exists(to_file)

print "Ready,hit RETURN to continue ,CTRL-C to abort."

raw_input()
#opening the file to start writing
#First opened out_file and put it in write mode
#You first want to open the file you want to write in
out_file = open(to_file,'w')
#write data from the first file into the second file
#writing data from the in_file to the out_file
#writing data from the in_file to the out_file
out_file.write(indata)

print "Alright, all done."

out_file.close()
#closes the file
in_file.close()

#amanyamarvin005@gmail.com
