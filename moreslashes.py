tabby_cat = "\tI'm tabbed in."
persian_cat = "I,m split\non a line."
backslash_cat = "I'm \\ a \\ cat."
sound = "\a\tsound."
fat_cat = """
I,ll do a list:
\tCat food
\tFishes
\tCatnip\n\tGrass
"""
print tabby_cat
print persian_cat
print backslash_cat
print fat_cat
print sound

